import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits import mplot3d
import time

def loss_fn(theta,X,y):
    m = len(y)
    return 1/(2*m)*np.linalg.norm((theta @ X) - y, axis = 1)**2

def grad(theta,X,y):
    m = len(y)
    return 1/m*(X @ (theta @ X - y))

# Generate data points
n_pts = 100
x = np.linspace(0,10,n_pts)

y = 75 - 5*x + 10*np.random.randn(n_pts)

# Set parameters and starting point
t = 0.01 # learning rate
n = 150 # number of iterations
gamma = 0.9 # "momentum" fraction

theta_0 = np.array([-75,-15]).astype('float')

# Generate meshgrid for contour plot
theta0 = np.linspace(-100,100,100)
theta1 = np.linspace(-20,40,100)

theta_grid = np.meshgrid(theta0, theta1)
theta_flat = np.stack((theta_grid[0].flatten(),theta_grid[1].flatten()),axis=1)

# Define matrix X
m = len(x)
X = np.ones((2,m))
X[1,:] = x

# Calculate loss function
z = loss_fn(theta_flat,X,y).reshape(100,100)

# Plot contour
fig = plt.figure()
ax = plt.axes()
ax.contour(theta_grid[0],theta_grid[1],z)
s = ax.text(plt.xlim()[1]-50,plt.ylim()[0]+2,"Iteration no.: ")

ax.set_xlabel(r'$\theta_0$')
ax.set_ylabel(r'$\theta_1$')

# Declare arrays to store theta at each step
theta_hist = np.zeros([n+1,2])
theta_m_hist = np.zeros([n+1,2])
theta_nag_hist = np.zeros([n+1,2])

# Set starting point
theta = np.copy(theta_0)
theta_m = np.copy(theta_0)
theta_nag = np.copy(theta_0)

theta_hist[0,:] = theta_0
theta_m_hist[0,:] = theta_0
theta_nag_hist[0,:] = theta_0

# Plot starting point
line, = ax.plot(theta[0], theta[1],'-xr',markersize=4, label = 'Gradient descent')
line_m, = ax.plot(theta_m[0], theta_m[1],'-og',markersize=4, label = 'Momentum')
line_nag, = ax.plot(theta_nag[0], theta_nag[1],'-*b',markersize=4, label = 'NAG')

ax.legend()

# Initialise update (delta) at step 0
delta = 0
delta_nag = 0

plt.pause(0.5)
input('Press ENTER to start...') 

# Normal gradient descent
for i in range(n):
    theta -= t*grad(theta,X,y)
    
    theta_hist[i+1,:] = theta
    
    line.set_xdata(theta_hist[:i+1,0])
    line.set_ydata(theta_hist[:i+1,1])
    s.set_text("Iteration no.: " + str(i+1))
    
    plt.draw()
    plt.pause(0.05)

input('Press ENTER to continue...')    
    
# Momentum
for i in range(n):
    delta = gamma*delta + t*grad(theta_m,X,y)
    theta_m -= delta
    
    theta_m_hist[i+1,:] = theta_m
    
    line_m.set_xdata(theta_m_hist[:i+1,0])
    line_m.set_ydata(theta_m_hist[:i+1,1])
    s.set_text("Iteration no.: " + str(i+1))

    plt.draw()
    plt.pause(0.05)

input('Press ENTER to continue...')

# Nesterov accelerated gradient
for i in range(n):    
    delta_nag = gamma*delta_nag + t*grad(theta_nag - gamma*delta_nag,X,y)
    theta_nag -= delta_nag
    
    theta_nag_hist[i+1,:] = theta_nag
    
    line_nag.set_xdata(theta_nag_hist[:i+1,0])
    line_nag.set_ydata(theta_nag_hist[:i+1,1])
    s.set_text("Iteration no.: " + str(i+1))
    
    plt.draw()
    plt.pause(0.05)

#ax.plot(theta[0], theta[1],'or')
input('Press ENTER to close...')