import numpy as np
import matplotlib.pyplot as plt
import time

x = np.linspace(-10,10,100)
y = np.square(x)

plt.show()

graph, ax = plt.subplots()

ax.plot(x,y)
ax.set_xlabel("$x$")
ax.set_ylabel("$f(x)$");

x0 = np.random.rand()*20-10
t = 0.01
niter = 100

x = x0

x_hist = np.array([x0])
line, = ax.plot(x_hist,x_hist**2,'-or')

plt.pause(1.5)

for i in range(niter):
    x -= t*2*x
    x_hist = np.append(x_hist,x)
    line.set_xdata(x_hist)
    line.set_ydata(x_hist**2)
    plt.draw()    
    plt.pause(0.01)
#    time.sleep(0.1)
    
plt.show()